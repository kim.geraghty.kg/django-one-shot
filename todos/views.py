from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from todos.models import TodoList, TodoItem

# Create your views here.


class TodoListView(ListView):
    model = TodoList
    template_name = "todo_list.html"


class TodoDetailView(DetailView):
    model = TodoList
    template_name = "todo_detail.html"


class TodoCreateView(CreateView):
    model = TodoList
    template_name = "todo_create.html"
    fields = ["name"]

    def form_valid(self, form):
        plan = form.save(commit=True)
        return redirect("todo_list_detail", pk=plan.id)


class TodoUpdateView(UpdateView):
    model = TodoList
    template_name = "todo_update.html"
    fields = ["name"]

    def form_valid(self, form):
        plan = form.save(commit=True)
        return redirect("todo_list_detail", pk=plan.id)


class TodoDeleteView(DeleteView):
    model = TodoList
    template_name = "todo_delete.html"
    success_url = reverse_lazy("todo_list_list")


class TodoItemCreateView(CreateView):
    model = TodoItem
    template_name = "item_create.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])


class TodoItemUpdateView(UpdateView):
    model = TodoItem
    template_name = "item_update.html"
    fields = ["task", "due_date", "is_completed", "list"]

    def get_success_url(self):
        return reverse_lazy("todo_list_detail", args=[self.object.list.id])
